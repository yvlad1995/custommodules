<?php

class Web_Qorder_Block_Quickorder extends Mage_Checkout_Block_Onepage_Abstract
{

    public function __construct()
    {
        return parent::__construct();
    }

    public function getFormAction()
    {
        return Mage::getUrl('quickorder/cart/post', array('_secure' => $this->getRequest()->isSecure()));
    }

    public function getMinAmount()
    {
        return Mage::getModel('webqorder/system_amount')->getMinimumAmount();
    }
    

}