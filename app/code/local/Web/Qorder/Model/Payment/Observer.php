<?php

class Web_Qorder_Model_Payment_Observer
{
    public function paymentMethodIsActive(Varien_Event_Observer $observer) {
        $event = $observer->getEvent();

        $method = $event->getMethodInstance();

        if($event->getQuote() == null || $method->getCode() != 'quickpayment')
        {
            return;
        }

        if(!empty($event->getQuote()->getQuickOrder()) && $event->getQuote()->getQuickOrder() == 'quickorder') return;

        $result = $event->getResult();
        $result->isAvailable = true;
        $hidePaymentMethods = array('quickpayment', 'webqorder');
        if (!empty($hidePaymentMethods)) {
            if (in_array($method->getCode(), $hidePaymentMethods)) {
                $result->isAvailable = false;
            }
        }
    }
}