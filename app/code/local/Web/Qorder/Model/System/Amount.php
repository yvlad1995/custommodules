<?php

class Web_Qorder_Model_System_Amount// extends Mage_Core_Model_Abstract
{
    public function getMinimumAmount()
    {
        return $this->getAmount();
    }

    protected function getAmount()
    {
        $minAmount = Mage::getStoreConfig('customqorder/customqorder_group/customqorder_amount');
        return (int)$minAmount;
    }
}