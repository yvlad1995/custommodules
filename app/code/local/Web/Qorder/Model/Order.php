<?php

class Web_Qorder_Model_Order extends Mage_Core_Model_Abstract
{

    protected $_order;
    protected $_store;
    protected $_quote;
    protected $_customer;
    protected $_websiteId;

    public function create($order)
    {
        $this->_order     = $order;
        $this->_websiteId = Mage::app()->getWebsite()->getId();
        $this->_store      = Mage::app()->getStore();

        //create customer or get customer
        $this->getCustomer();
        $this->createOrder();
    }

    protected function createOrder()
    {
        // Start New Sales Order Quote
        $this->_quote = Mage::getModel('sales/quote')->setStoreId($this->_store->getId());

        // Set Sales Order Quote Currency
        $this->_quote->setCurrency($this->_store->getCurrentCurrencyCode());

        // Assign Customer To Sales Order Quote
        $this->_quote->assignCustomer($this->_customer);

        $cart = $this->getCart();
        foreach($cart as $item){
            $product = $this->getProducts()->load($item->getProductId());

            $this->_quote->addProduct($product, new Varien_Object(array('qty'   => $item->getQty())));
        }

        //Billing and shipping
        $this->_quote->getBillingAddress()->addData($this->getHelper()->getBillingData($this->_order));

        $this->_quote->getShippingAddress()->addData($this->getHelper()->getShippingData($this->_order));

        $this->_quote->setQuickOrder('quickorder');
        
        $this->_quote->getShippingAddress()
            ->setShippingMethod('quickshipping_quickshipping')
            ->setCollectShippingRates(true)
            ->setPaymentMethod('quickpayment');

//         Set Sales Order Payment
        $this->_quote->getPayment()->importData(array('method' => 'quickpayment'));

        // Collect Totals & Save Quote

        $this->_quote->collectTotals()->save();

        // Create Order From Quote
        $service = Mage::getModel('sales/service_quote', $this->_quote);
        $service->submitAll();

        //for success

        Mage::getSingleton('checkout/session')
            ->setLastQuoteId($this->_quote->getId())
            ->setLastOrderId($service->getOrder()->getId())
            ->setLastSuccessQuoteId($service->getOrder()->getId());

        $quoteItems = Mage::getSingleton('checkout/session')
            ->getQuote()
            ->getItemsCollection();

//        foreach($quoteItems as $item)
//        {
//            Mage::getSingleton('checkout/cart')->removeItem($item->getId());
//        }
//        Mage::getSingleton('checkout/cart')->save();
    }

    protected function getCustomer()
    {
        $this->_customer = Mage::getModel('customer/customer')
            ->setWebsiteId($this->_websiteId)
            ->loadByEmail($this->_order->getEmail());

        if($this->_customer->getId() == ""){

            $this->_customer = Mage::getModel('customer/customer');
            $this->_customer->setWebsiteId($this->_websiteId)
                ->setStore($this->_store)
                ->setFirstname($this->_order->getName())
                ->setLastname($this->_order->getLastname())
                ->setEmail($this->_order->getEmail());
            $this->_customer->save();
        }

    }

    protected function getHelper()
    {
        return Mage::helper('webqorder');
    }

    protected function getCart()
    {
        return Mage::getModel('checkout/cart')->getQuote()->getAllItems();
    }

    protected function getProducts()
    {
        return Mage::getModel('catalog/product');
    }

}