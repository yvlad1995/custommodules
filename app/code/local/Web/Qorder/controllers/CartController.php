<?php

class Web_Qorder_CartController extends Mage_Checkout_Controller_Action
{
    public function postAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_redirect('/');
            return;
        }

        $request = $this->getRequest()->getPost();
        
        if(!Mage::getSingleton('customer/session')->isLoggedIn())
        {
            if(empty($request['email']))
            {
                Mage::getSingleton('checkout/session')->addError(Mage::helper('contacts')->__('Please enter email and telephone'));
                $this->_redirect('checkout/cart/');
                return;
            }

            if(empty($request['telephone'])) $request['telephone'] = 'no phone';

            $data = new Varien_Object();
            $data->setData($request);
            $data->setFirstname($request['firstname']);
            $data->setLastname($request['lastname']);
            $data->setTelephone($request['telephone']);

        }else{
            $data = Mage::getModel('customer/customer')->load(Mage::getSingleton('customer/session')->getId());
        }
        
        if($data){
            try{
                $order = Mage::getModel('webqorder/order');
                $order->create($data);
                $this->_redirect('checkout/onepage/success/');
                return;
            }catch(Exception $e){
                Mage::getSingleton('checkout/session')->addError(Mage::helper('contacts')->__('Please complete your order through the checkout!'));
                $this->_redirect('checkout/cart/');
                return;
            }
        }
    }
}