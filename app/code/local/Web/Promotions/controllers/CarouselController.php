<?php

class Web_Promotions_CarouselController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function frontAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function categoryAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function getBlockAction()
    {
        $id = $this->getRequest()->getParam('id');

        $first = max(0, intval($this->getRequest()->getParam('first')) - 1);
        $last  = max($first + 1, intval($this->getRequest()->getParam('last')) - 1);
        $length = $last - $first + 1;

        try{
            $data = Mage::helper('webpromotions')->getCmsBlocks($id);
            $blocks = array();
            foreach($data['block_id'] as $id)
                $blocks[] = $this->getLayout()->createBlock('cms/block')->setBlockId($id)->toHtml();//Mage::getSingleton('cms/block')->load($id)->getData();
            $total    = count($blocks);
            $selected = array_slice($blocks, $first, $length);

            echo json_encode(array(
                'n'      => $total,
                'result' => $selected
            ));
        }catch(Exception $e)
        {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    public function getPromotionsAction()
    {
        try{
            $first = max(0, intval($this->getRequest()->getParam('first')) - 1);
            $last  = max($first + 1, intval($this->getRequest()->getParam('last')) - 1);

            $images = Mage::helper('webpromotions/image')->getImagePromotions($first, $last);

            echo json_encode(array(
                'n'         => $images['n'],
                'result'    => $images['result']
            ));
        }catch (Exception $e)
        {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }
}