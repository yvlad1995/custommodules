<?php


class Web_Promotions_Helper_SerializeDecode extends Mage_Core_Helper_Js
{
    public function decodeGridSerializedInput($encoded)
    {
        $isSimplified = (false === strpos($encoded, '='));
        $result = array();
        parse_str($encoded, $decoded);
        foreach($decoded as $key => $value) {
            if (is_numeric($key)) {
                if ($isSimplified) {
                    $result[$key] = $key;
                } else {
                    $result[$key] = null;
                    parse_str(base64_decode($value), $result[$key]);
                }
            }
        }
        return $result;
    }
}