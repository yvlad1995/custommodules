<?php

class Web_Promotions_Helper_Front
{
    public function getPromotionsData($id)
    {
        try{
            $promotions   = Mage::getModel('webpromotions/promotions')->load($id);
            $staticBlocks = unserialize($promotions->getStaticBlocks());
            $front        = Mage::getUrl() . "webpromotions/carousel/front/id/";

            $image = '';

            if($promotions['image'] && $promotions['image'] != ''){
                $image        = $promotions->getImage();
            }

            $data = array(
                'id'              => $promotions->getId(),
                'promotions_name' => $promotions->getPromotionsName(),
                'description'     => $promotions->getDescription(),
                'static_blocks'   => $staticBlocks,
                'image'           => $image,
                'front'           => $front
            );

            return $data;
            
        }catch(Exception $e){
            return false;
        }

    }
}
