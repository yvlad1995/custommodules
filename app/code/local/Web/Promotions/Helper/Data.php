<?php

class Web_Promotions_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_cmsblockId;
    public function getImagePath($id = 0)
    {
        $path = Mage::getBaseDir('media') . '/web_promotions';
        if ($id) {
            return "{$path}/{$id}.jpg";
        } else {
            return $path;
        }
    }

    public function getImageUrl($id = 0)
    {
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'web_promotions/';
        if ($id) {
            return $url . $id . '.jpg';
        } else {
            return $url;
        }
    }

    public function getPromotionsData($categoryId)
    {
        $connection = Mage::getModel('core/resource')->getConnection('core_read');
        $query      = "Select * from `web_promotions_category`";
        $rows       = $connection->fetchAll($query);
        
        $promotionsData = array();
        foreach ($rows as $value)
        {
            if($value['category_id'] == $categoryId)
            {
                $promotionsData[] = Mage::getModel('webpromotions/promotions')->load($value['promotion_id']);
            }
        }
        
        return $promotionsData;
    }

    public function getCmsblockId($id)
    {
        $this->_cmsblockId = $id;
        $collection = $this->getCmsblockById();

        foreach($collection as $block){
            $blockId[] = $block->getBlockId();
        }
        if(empty($blockId)){
            return $blockId = array('0');
        }
        return $blockId;
    }

    public function getCmsBlocks($id)
    {
        $promotions   = Mage::getModel('webpromotions/promotions')->load($id);

        return array(
                    'block_id'   => $this->getCmsblockId($id),
                    'promotions' => $promotions
                    );
    }

    protected function getCmsblockById()
    {
        $collection = Mage::getModel('cms/block')->getCollection();

        $collection->getSelect()
            ->joinLeft(
                array('p' => $collection->getTable('webpromotions/promotions_cmsblock')),
                "main_table.block_id = p.block_id",
                array()
            )
            ->where(
                'p.promotion_id=?' , $this->_cmsblockId
            );
        return $collection;
    }
}