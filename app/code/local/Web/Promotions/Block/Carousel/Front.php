<?php

class Web_Promotions_Block_Carousel_Front extends Mage_Core_Block_Template
{

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getPromotionsCollection()
    {
        $id = $this->getRequest()->getParam('id');

        return Mage::helper('webpromotions')->getCmsBlocks($id);
    }

    public function getDataPromotions()
    {
        $data = $this->getPromotionsCollection();
        try{
            $blocks = array();
            $i = 0;
            foreach($data['block_id'] as $id){
                if($id != '0' && $i < 4){
                    $blocks_id[] = $id;
                }
                $i++;
            }
            
//            $dataPromotion = array(
//                'promotions_name' => $data['promotions_name'],
//                'description'      => $data['description'],
//            );
            return array(
                'blocks'          => $blocks_id,
                'promotions_name' => $data['promotions']->getPromotionsName(),
                'description'     => $data['promotions']->getDescription()
            );

        }catch(Exception $e)
        {
            return false;
        }
    }

    protected function getCategory($category_id)
    {
        foreach ($category_id as $id) {
            $category[] = Mage::getSingleton('catalog/category')->load($id)->getData();
        }
    }

    public function getPromotions()
    {
        try{
            $promotionsData = Mage::helper('webpromotions')->getPromotionsData($this->getCategoryId());

            return $promotionsData;
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    public function getFront()
    {
        return Mage::getUrl() . "webpromotions/carousel/front/id/";
    }

    protected function getCategoryId()
    {
        return Mage::registry('current_category') ? Mage::registry('current_category')->getId() : null;
    }

    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();    
    }

    public function count()
    {
        $i = 0;
        foreach($this->getLoadedProductCollection() as $item)
        {
            if($item->getVisibility() != 1 && $item->getVisibility() != 3)
            $i++;
        }

        if($i == 0)
        {
            return false;
        }

        return true;
    }

    protected function _getProductCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
                                                ->addAttributeToSelect('image')
                                                ->addAttributeToSelect('visibility');

        $collection->joinTable(
            'webpromotions/promotions_product',
            'product_id=entity_id',
            array('product_id'=>'product_id'),
            'promotion_id=' . $this->getRequest()->getParam('id'),
            'right');

        return $collection;
    }



}
