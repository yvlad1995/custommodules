<?php

class Web_Promotions_Block_Adminhtml_Promotions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    protected function _prepareCollection() 
    {
        $collection = Mage::getModel('webpromotions/promotions')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns() 
    {
        $helper = Mage::helper('webpromotions');

        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index'  => 'id',
            'type'   => 'number'
        ));

        $this->addColumn('image', array(
            'header'  => $helper->__('Image'),
            'align'   => 'left',
            'index'   => 'image',
            'width'   => '97',
            'renderer'=> 'Web_Promotions_Block_Adminhtml_Image_Content',
            'type'    => 'image',
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index'  => 'promotions_name',
            'type'   => 'text',
        ));
         
        $this->addColumn('Description', array(
            'header'  => $helper->__('Description'),
            'index'   => 'description',
            'type'    => 'textarea',
        ));


        return parent::_prepareColumns();
    }
    
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('promotions');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete')
        ));
        return $this;
    }
    
    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
                    'id' => $model->getId(),
                ));
    }
    

}