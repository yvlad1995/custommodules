<?php

class Web_Promotions_Block_Adminhtml_Image_Content extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {

        $mediaurl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $value = $row->getData($this->getColumn()->getIndex());
        
        if($value != ''){
            $out = "<img src=". $value ." width='150px'/>";
            return $out;
        }
        return;
    }
}