<?php

class Web_Promotions_Model_Promotions_Product
    extends Mage_Core_Model_Abstract {

    protected function _construct(){
        $this->_init('webpromotions/promotions_product');
    }

    public function savePromotionsRelation($promotions){
            $data = $promotions->getProductsData();
            if (!is_null($data)) {
                $this->_getResource()->savePromotionsRelation($promotions, $data);
            }
            return $this;
    }

    public function getProductCollection($promotions){
        $collection = Mage::getResourceModel('webpromotions/promotions_product_collection')
            ->addPromotionsFilter($promotions);
         return $collection;
    }
}