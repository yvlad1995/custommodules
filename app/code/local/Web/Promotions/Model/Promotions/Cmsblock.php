<?php

class Web_Promotions_Model_Promotions_Cmsblock extends Mage_Core_Model_Abstract {

    protected function _construct()
    {
        $this->_init('webpromotions/promotions_cmsblock');
    }

    public function savePromotionRelation($promotion)
    {
        $data = $promotion->getCmsblockData();
        if (!is_null($data)) {
            $this->_getResource()->savePromotionRelation($promotion, $data);
        }
        return $this;
    }

    public function getCmsBlockCollection($promotion)
    {
        $collection = Mage::getResourceModel('webpromotions/promotions_cmsblock_collection')
            ->addPromotionFilter($promotion);
            return $collection;
    }
}