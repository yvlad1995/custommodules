<?php

class Web_Promotions_Model_Promotions extends Mage_Core_Model_Abstract
{
    protected $_productInstance = null;
    protected $_categoryInstance = null;
    protected $_cmsblockInstance = null;

    public function _construct()
    {
        parent::_construct();
        $this->_init('webpromotions/promotions');
    }

    protected function _afterDelete()
    {
        $helper = Mage::helper('webpromotions');
        @unlink($helper->getImagePath($this->getId()));
        return parent::_afterDelete();
    }

    public function getImageUrl()
    {
        $helper = Mage::helper('webpromotions');

        if ($this->getId() && file_exists($helper->getImagePath($this->getId()))) {
            return $helper->getImageUrl($this->getId());
        }
        return null;
    }


    public function getProductInstance()
    {
        if (!$this->_productInstance)
        {
            $this->_productInstance = Mage::getSingleton('webpromotions/promotions_product');
        }
        return $this->_productInstance;
    }

    protected function _afterSave()
    {
        $this->getProductInstance()->savePromotionsRelation($this);
        $this->getCategoryInstance()->savePromotionRelation($this);
        $this->getCmsblockInstance()->savePromotionRelation($this);
        return parent::_afterSave();
    }

    public function getSelectedProducts()
    {
        if (!$this->hasSelectedProducts())
        {
            $products = array();
            foreach ($this->getSelectedProductsCollection() as $product)
            {
                $products[] = $product;
            }
            $this->setSelectedProducts($products);
        }
        return $this->getData('selected_products');
    }
    public function getSelectedProductsCollection()
    {
        $collection = $this->getProductInstance()->getProductCollection($this);
        return $collection;
    }

    public function getProductsPosition()
    {
        if (!$this->getId())
        {
            return array();
        }

        $array = null;
        if (is_null($array))
        {
            $array = $this->getResource()->getProductsPosition($this);
            $this->setData('products_position', $array);
        }
        return $array;
    }

    public function getCategoryInstance()
    {
        if (!$this->_categoryInstance)
        {
            $this->_categoryInstance = Mage::getSingleton('webpromotions/promotions_category');
        }
        return $this->_categoryInstance;
    }

    public function getSelectedCategories()
    {
        if (!$this->hasSelectedCategories())
        {
            $categories = array();
            foreach ($this->getSelectedCategoriesCollection() as $category)
            {
                $categories[] = $category;
            }
            $this->setSelectedCategories($categories);
        }
        return $this->getData('selected_categories');
    }

    public function getSelectedCategoriesCollection()
    {
        $collection = $this->getCategoryInstance()->getCategoryCollection($this);
        return $collection;
    }

    public function getCmsblockInstance()
    {
        if (!$this->_cmsblockInstance)
        {
            $this->_cmsblockInstance = Mage::getSingleton('webpromotions/promotions_cmsblock');
        }
        return $this->_cmsblockInstance;
    }


}