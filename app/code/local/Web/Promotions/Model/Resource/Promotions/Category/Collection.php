<?php

class Web_Promotions_Model_Resource_Promotions_Category_Collection
    extends Mage_Catalog_Model_Resource_Category_Collection{

    protected $_joinedFields = false;

    public function joinFields(){
        if (!$this->_joinedFields){
            $this->getSelect()->join(
                array('related' => $this->getTable('webpromotions/promotions_category')),
                'related.category_id = e.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addPromotionFilter($promotion){
        if ($promotion instanceof Web_Promotions_Model_Promotions){
            $promotion = $promotion->getId();
        }
        if (!$this->_joinedFields){
            $this->joinFields();
        }
        $this->getSelect()->where('related.promotion_id = ?', $promotion);
        return $this;
    }
}