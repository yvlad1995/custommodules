<?php

$installer = $this;
$table = $installer->getTable('webpromotions/promotions');

$installer->startSetup();

$installer->getConnection()->dropTable($table);

$tablePromotions = $installer->getConnection()
    ->newTable($table)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ))
    ->addColumn('promotions_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ))
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('is_enabled', Varien_Db_Ddl_Table::TYPE_TINYINT, '255', array(
        'nullable'  => false,
        ));
$installer->getConnection()->createTable($tablePromotions);

$installer->getConnection()->dropTable($this->getTable('webpromotions/promotions_product'));
$table = $this->getConnection()
    ->newTable($this->getTable('webpromotions/promotions_product'))
    ->addColumn('promotion_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Promotion ID')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Product ID')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Position')
    ->addIndex($this->getIdxName('webpromotions/promotions_product', array('product_id')), array('product_id'))
    ->addForeignKey($this->getFkName('webpromotions/promotions_product', 'promotion_id', 'webpromotions/promotions', 'id'),
        'promotion_id', $this->getTable('webpromotions/promotions'), 'id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($this->getFkName('webpromotions/promotions_product', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $this->getTable('catalog/product'), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Promotion to Product Linkage Table');

$this->getConnection()->createTable($table);

$installer->getConnection()->dropTable($this->getTable('webpromotions/promotions_category'));
$table = $this->getConnection()
    ->newTable($this->getTable('webpromotions/promotions_category'))
    ->addColumn('rel_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Relation ID')
    ->addColumn('promotion_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Promotion ID')
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Category ID')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Position')
    ->addIndex($this->getIdxName('webpromotions/promotions_category', array('category_id')), array('category_id'))
    ->addForeignKey($this->getFkName('webpromotions/promotions_category', 'promotion_id', 'webpromotions/promotions', 'id'), 'promotion_id', $this->getTable('webpromotions/promotions'), 'id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($this->getFkName('webpromotions/promotions_category', 'category_id', 'catalog/category', 'entity_id'),    'category_id', $this->getTable('catalog/category'), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addIndex(
        $this->getIdxName(
            'webpromotions/promotions_category',
            array('promotion_id', 'category_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('promotion_id', 'category_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->setComment('Promotion to Category Linkage Table');
$this->getConnection()->createTable($table);

$installer->endSetup();