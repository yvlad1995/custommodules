<?php

class Custom_CmsMenu_Adminhtml_CmslinksController extends Mage_Adminhtml_Controller_Action
{
    
    public function indexAction()
    {  
        $this->_redirect('*/adminhtml_cmsmenu/edit/id/' . $this->getRequest()->getParam('menu_id'));
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }

    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('cms/page/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('cms/page/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('cms/page');
                break;
        }
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('link_id');
        $model = Mage::getModel('customcmsmenu/cmslinks');

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $model->setData($data)->setId($id);
        }else{
            $model->load($id);
        }

        Mage::register('current_cmslinks', $model);   
        
        $this->loadLayout()->_setActiveMenu('cms');
        $this->_addContent($this->getLayout()->createBlock('customcmsmenu/adminhtml_cmslinks_edit'));

        $this->renderLayout();
    }
   
    public function saveAction()
    {
        if($data = $this->getRequest()->getPost()){
            try{
                $model = Mage::getModel('customcmsmenu/cmslinks');
                $model->setData($data)->setLinksId($this->getRequest()->getParam('link_id'))
                      ->setMenuId($this->getRequest()->getParam('menu_id'));
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Link was \'%s\' succesfuly saved', $model->getLabel()));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if($this->getRequest()->getParam('back'))
                {
                    $this->_redirect('*/*/edit/id/' . $model->getId());
                    return;
                }
                $this->_redirect('*/adminhtml_cmsmenu/edit', array(
                    'id' => $this->getRequest()->getParam('menu_id')
                ));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
               
                $this->_redirect('*/adminhtml_cmsmenu/edit' , array(
                    'id'    => $this->getRequest()->getParam('menu_id'),
                ));
            }
            return;
        }
        Mage::getSinglton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        
        $this->_redirect('*/adminhtml_cmsmenu/');
    }

    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('link_id')){
            try{
                Mage::getModel('customcmsmenu/cmslinks')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Link was deleted successfully'));
                $this->_redirect('*/adminhtml_cmsmenu/edit' , array('id' => $this->getRequest()->getParam('menu_id')));
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/adminhtml_cmsmenu/edit', array('id' => $this->getRequest()->getParam('menu_id')));
            }
        }
    }

    public function massDeleteAction()
    {
        $links = $this->getRequest()->getPost('link');

        if(is_array($links) && sizeof($links) > 0){
            try {
                foreach ($links as $id){
                    Mage::getModel('customcmsmenu/cmslinks')->setLinksId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d link(s) have been deleted', count($links)));
            } catch (Exception $e) {
                $this->_getSesion()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select menu'));
        }
        $this->_redirect('*/adminhtml_cmsmenu/edit/id', array(
            'id' => $this->getRequest()->getParam('id')
        ));
    }
}