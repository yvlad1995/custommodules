<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    protected function _prepareCollection() 
    {
        $collection = Mage::getModel('customcmsmenu/cmsmenu')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns() 
    {
        $helper = Mage::helper('customcmsmenu');

        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index'  => 'id',
            'type'   => 'number'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'menu_name',
            'type' => 'text',
        ));

        $this->addColumn('data_created', array(
            'header' => $helper->__('Date Created'),
            'index' => 'created',
            'type' => 'date',
        ));
        
        $this->addColumn('data_update', array(
            'header' => $helper->__('Date Updated'),
            'index' => 'updated',
            'type' => 'date',
        ));
         
        $this->addColumn('status', array(
            'header'  => $helper->__('Status'),
            'index'   => 'status',
            'type'    => 'options',
            'options' => array(
                '0' => 'Disabled',
                '1' => 'Enabled'
            )
        ));

        return parent::_prepareColumns();
    }
    
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('menu');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete')
        ));
        return $this;
    }
    
    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
                    'id' => $model->getId(),
                ));
    }
    

}