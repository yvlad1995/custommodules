<?php

class Custom_Bonus_Block_Html_Welcome extends Mage_Core_Block_Template
{
    /**
     * Get customer session
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get block message
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (empty($this->_data['welcome'])) {
            if (Mage::isInstalled() && $this->_getSession()->isLoggedIn()) {
                $this->_data['welcome'] = $this->__('Welcome, %s!', $this->escapeHtml($this->_getSession()->getCustomer()->getName()));
                if(!empty(Mage::getModel('custombonus/bonus')->getBonus(Mage::getSingleton('customer/session')->getId()))) {
                    $this->_data['welcome'] .= $this->__('<br />Bonus Payment: %s',
                        $this->escapeHtml(Mage::getModel('custombonus/bonus')->getBonus(Mage::getSingleton('customer/session')->getId())));
                    
                    $currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
                    $this->_data['welcome'] .= $this->__(' ' . $this->escapeHtml(Mage::app()->getLocale()->currency($currency_code)->getSymbol()));
                }
            } else {
                $this->_data['welcome'] = Mage::getStoreConfig('design/header/welcome');
            }
        }

        return $this->_data['welcome'];
    }

    /**
     * Get tags array for saving cache
     *
     * @return array
     */
    public function getCacheTags()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->addModelTags($this->_getSession()->getCustomer());
        }

        return parent::getCacheTags();
    }
}