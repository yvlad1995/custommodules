<?php

class Custom_Bonus_Model_Observer
{
   public function editCreditmemo($observer)
   {

       $orderId = $observer->getData('request')->getParams();
       $orderObject = $this->getOrder($orderId['order_id']);

       $paymentMethod = $orderObject->getPayment()->getMethodInstance()->getCode();

       if($paymentMethod !== 'custombonus'){
           return;
       }

       $invoiceIds = $orderObject->getInvoiceCollection()->getItems();

       foreach($invoiceIds as $item) {
           $invoice = $item;
       }

       $creditmemo = $observer->getCreditmemo();
       $creditmemo->setInvoice($invoice);

       return $creditmemo;
   }

    protected function getOrder($orderId)
    {
        return Mage::getModel('sales/order')->load($orderId);
    }
}


        

